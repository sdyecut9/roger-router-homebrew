# Roger Router macOS

## Installation

Use Homebrew to install Roger Router. 

For this, install [Homebrew](https://brew.sh) itself, then install Roger Router using Terminal.app:

```
brew tap tabos/rogerrouter https://gitlab.com/tabos/roger-router-homebrew.git
brew install --build-from-source capi20 librm rogerrouter
```

All dependencies should get installed automatically.

To run roger, type "roger" in the terminal.

To auto-start Roger Router on login, add /usr/local/bin/roger to your autostarts in your User preferences.

## Important note for existing users

Please be advised that **our Homebrew tap location changed**. In order to update your Homebrew setup please run the following:

```
brew untap -f tabos/rogerrouter
brew tap tabos/rogerrouter https://gitlab.com/tabos/roger-router-homebrew.git
```

Your installed software will not be affected by this and you'll receive updates as usual again.

## Support Roger Router

We'd love to integrate Roger Router directly in Homebrew Core. To do this we need **your** help.
If you use Roger Router via the Homebrew installation above we kindly ask you to _Star_ the following three repositories:

- https://gitlab.com/tabos/rogerrouter
- https://gitlab.com/tabos/librm
- https://gitlab.com/tabos/libcapi

By hitting the _Star_ button (upper right corner of project page) of those three projects you increase their visible popularity. We need _75 Stars_ to integrate Roger Router with Homebrew directly. Such direct integration would mean a simpler installation (no more tap) and no more building from source - for everyone.

Thanks for your support!
